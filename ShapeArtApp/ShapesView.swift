//
//  ViewController.swift
//  ShapeArtApp
//
//  Created by Afsara on 11/04/18.
//  Copyright © 2018 Wavalabs. All rights reserved.
//


import UIKit
import Foundation

class ShapesView {

    
    func drawCustomRectangleImage(_ size: CGSize, color: UIColor) -> UIImage {
        // Setup our context
        let bounds = CGRect(origin: CGPoint.zero, size: size)
        UIGraphicsBeginImageContext(size)
        let context = UIGraphicsGetCurrentContext()
        
        context?.beginPath()
        context?.move(to: CGPoint(x: bounds.minX, y: bounds.maxY/4))
        context?.addLine(to: CGPoint(x: bounds.minX, y: bounds.maxY*3/4))
        context?.addLine(to: CGPoint(x: bounds.maxX, y: bounds.maxY*3/4))
        context?.addLine(to: CGPoint(x: bounds.maxX, y: bounds.maxY/4))
        context?.closePath()
        context?.setFillColor(color.cgColor)
        context?.fillPath()
        
        // Drawing complete, retrieve the finished image and cleanup
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    
    
    func drawCustomEllipseImage(_ size: CGSize, color: UIColor) -> UIImage {
        // Setup our context
        let bounds = CGRect(origin: CGPoint.zero, size: size)
        UIGraphicsBeginImageContext(size)
        let context : CGContext = UIGraphicsGetCurrentContext()!
        
        context.addEllipse(in: bounds)
        context.setFillColor(color.cgColor)
        context.fillPath()
        
        // Drawing complete, retrieve the finished image and cleanup
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    func drawCustomTriangleImage(_ size: CGSize, color: UIColor) -> UIImage {
        // Setup our context
        let bounds = CGRect(origin: CGPoint.zero, size: size)
        UIGraphicsBeginImageContext(size)
        let context : CGContext = UIGraphicsGetCurrentContext()!
        context.beginPath()
        context.move(to: CGPoint(x: bounds.minX, y: bounds.maxY))
        context.addLine(to: CGPoint(x: bounds.maxX, y: bounds.maxY))
        context.addLine(to: CGPoint(x: (bounds.maxX/2.0), y: bounds.minY))
        context.closePath()
        context.setFillColor(color.cgColor)
        context.fillPath()
        
        // Drawing complete, retrieve the finished image and cleanup
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    func drawCustomStarImage(_ size: CGSize, color: UIColor) -> UIImage {
        // Setup our context
        let bounds = CGRect(origin: CGPoint.zero, size: size)
        UIGraphicsBeginImageContext(size)
        let context : CGContext = UIGraphicsGetCurrentContext()!
        context.beginPath()
        context.move(to: CGPoint(x: bounds.minX, y: bounds.maxY/2.67))
        context.addLine(to: CGPoint(x: bounds.maxX/3.5, y: bounds.maxY/1.6))
        context.addLine(to: CGPoint(x: bounds.maxX/5, y: bounds.maxY))
        context.addLine(to: CGPoint(x: bounds.maxX/2, y: bounds.maxY/1.28))
        context.addLine(to: CGPoint(x: bounds.maxX/1.25, y: bounds.maxY))
        context.addLine(to: CGPoint(x: bounds.maxX/1.39, y: bounds.maxY/1.6))
        context.addLine(to: CGPoint(x: bounds.maxX, y: bounds.maxY/2.67))
        context.addLine(to: CGPoint(x: bounds.maxX/1.54, y: bounds.maxY/2.67))
        context.addLine(to: CGPoint(x: bounds.maxX/2.0, y: bounds.minY))
        context.addLine(to: CGPoint(x: bounds.maxX/2.8, y: bounds.maxY/2.67))
        
        context.closePath()
        context.setFillColor(color.cgColor)
        context.fillPath()
        
        // Drawing complete, retrieve the finished image and cleanup
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
   
    func pointFrom(_ angle: CGFloat, radius: CGFloat, offset: CGPoint) -> CGPoint {
        return CGPoint(x: radius * cos(angle) + offset.x, y: radius * sin(angle) + offset.y)
    }
}

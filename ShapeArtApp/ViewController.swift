//
//  ViewController.swift
//  ShapeArtApp
//
//  Created by Afsara on 11/04/18.
//  Copyright © 2018 Wavalabs. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPopoverPresentationControllerDelegate, ShapeSelectionDelegate, UIGestureRecognizerDelegate {

    
    var selectedImageView : UIImageView!
    var location = CGPoint(x: 0, y: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let leftBarButton = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(leftBarButtonAction(sender:)))
        self.navigationItem.leftBarButtonItem = leftBarButton
        
    }

    @objc func leftBarButtonAction(sender: UIBarButtonItem){

        let buttonItemView = sender.value(forKey: "view") as? UIView
        let popController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "popOverView") as! ShapesListViewController
        
        // set the presentation style
        popController.modalPresentationStyle = UIModalPresentationStyle.popover
        
        // set up the popover presentation controller
        popController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.up
        popController.popoverPresentationController?.delegate = self
        popController.popoverPresentationController?.sourceView = buttonItemView as? UIView // button
        popController.popoverPresentationController?.sourceRect = (buttonItemView?.bounds)!
        
        // present the popover
        self.present(popController, animated: true, completion: nil)
        
        popController.selectionDelegate = self

    }

    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        // return UIModalPresentationStyle.FullScreen
        return UIModalPresentationStyle.none
    }
    
    
    func didSelectShape(name: String) {
        print("selection screen name ", name);
        self.presentedViewController?.dismiss(animated: true, completion: nil)

        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        let pinchGuestureRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(imagePinch(pinchGestureRecognizer:)))
        
        tapGestureRecognizer.delegate = self
        pinchGuestureRecognizer.delegate = self
        
        var imageWidth : CGFloat = 200
        var imageHeight : CGFloat = 200
        
        if(name == "Ellipse"){
            imageWidth = 200
            imageHeight = 100
        }
        
        
        var shapeImgView : UIImageView
        shapeImgView  = UIImageView(frame:CGRect(x:self.view.frame.width/2, y:self.view.frame.width/2, width:imageWidth, height:imageHeight));

        
        if(name == "Rectangle"){
            shapeImgView.tag = 101
            shapeImgView.image = ShapesView().drawCustomRectangleImage(CGSize(width: 200, height: 200), color: UIColor.blue)

        }else if(name == "Triangle"){
            shapeImgView.tag = 102
            shapeImgView.image = ShapesView().drawCustomTriangleImage(CGSize(width: 200, height: 200), color: UIColor.purple)

        }else if(name == "Star"){
            shapeImgView.tag = 103
            shapeImgView.image = ShapesView().drawCustomStarImage(CGSize(width: 200, height: 200), color: UIColor.gray)

        }else if(name == "Ellipse"){
            shapeImgView.tag = 104
            shapeImgView.image = ShapesView().drawCustomEllipseImage(CGSize(width: 200, height: 300), color: UIColor.cyan)
        }
        
        shapeImgView.isUserInteractionEnabled = true
        shapeImgView.addGestureRecognizer(tapGestureRecognizer)
        shapeImgView.addGestureRecognizer(pinchGuestureRecognizer)
        self.view.addSubview(shapeImgView)

    }
    
    
    
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        let tappedImageTag = tappedImage.tag
        // Your action
        
        for subview in self.view.subviews {
            if(tappedImageTag == subview.tag){
                selectedImageView = subview as! UIImageView
                break
            }
        }
    }
    
    @objc func imagePinch(pinchGestureRecognizer: UIPinchGestureRecognizer){
        pinchGestureRecognizer.view?.transform = view.transform.scaledBy(x: pinchGestureRecognizer.scale, y: pinchGestureRecognizer.scale)
        pinchGestureRecognizer.scale = 1
        
    }
    

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch : UITouch! = touches.first
        location = touch.location(in: self.view)
        
        if(touch.view?.tag == 101 || touch.view?.tag == 102 || touch.view?.tag == 103 || touch.view?.tag == 104){
            touch.view?.center = location
        }
        
    }
    
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch : UITouch! = touches.first
        location = touch.location(in: self.view)
        if(touch.view?.tag == 101 || touch.view?.tag == 102 || touch.view?.tag == 103 || touch.view?.tag == 104){
            touch.view?.center = location
        }

    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
            return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}


//
//  ShapesListViewController.swift
//  ShapeArtApp
//
//  Created by Afsara on 12/04/18.
//  Copyright © 2018 Wavalabs. All rights reserved.
//

import UIKit

protocol ShapeSelectionDelegate {
    func didSelectShape(name : String)
}

class ShapesListViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
   
    var selectionDelegate: ShapeSelectionDelegate!
    

    @IBOutlet weak var shapesListTableview: UITableView!
    
    var ShapesArray = ["Ellipse","Rectangle","Triangle","Star"]

    
    override func viewDidLoad() {
        super.viewDidLoad()

        shapesListTableview.delegate = self
        shapesListTableview.dataSource = self
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ShapesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath as IndexPath) as UITableViewCell
        cell.textLabel?.text = ShapesArray[indexPath.row]
        return cell
   }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let indexPath = tableView.indexPathForSelectedRow //optional, to get from any UIButton for example
        let name =  ShapesArray[(indexPath?.row)!]
        
        print("Selected in list ", name )
        
        if(selectionDelegate != nil){
            selectionDelegate!.didSelectShape(name: name)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
